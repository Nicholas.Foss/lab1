package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
        while (true) {

            //To start the round
            System.out.printf("Let's play round %d%n", roundCounter);
            roundCounter++;

            //To get the choices right
            String humanChoice = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
            Random rand = new Random();
            String computerChoice = rpsChoices.get(rand.nextInt(3));
            while (!validChoice(humanChoice)){
                System.out.printf("I do not understand %s. Could you try again?%n", humanChoice);
                humanChoice = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
            }
            
            //Announce the results
            String resultStatement = "Human chose " + humanChoice + ", computer chose " + computerChoice + ". ";
            String result = rpsResult(humanChoice, computerChoice);
            System.out.println(resultStatement + result);

            //Update leaderboard
            if (result.equals("Human wins!")){
                humanScore++;
            }
            else if (result.equals("Computer wins!")){
                computerScore++;
            }

            System.out.printf("Score: human %d, computer %d%n", humanScore, computerScore);

            //Continue to play?
            String continueToPlay = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();
            while (!continueToPlay.equals("y") && !continueToPlay.equals("n")){
                System.out.printf("I do not understand %s. Could you try again?%n", continueToPlay);
                continueToPlay = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();
            }
            
            if (continueToPlay.equals("y")){
                continue;
            }
            else{
            System.out.println("Bye bye :)");
            break;
            }

        }
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

    public boolean validChoice (String choice) {
        for (String s : rpsChoices){
            if (choice.equals(s)){
                return true;
            }
        }
        return false;
        
    }

    public String rpsResult (String hc, String cc){
        if (hc.equals(cc)){
            return "It's a tie!";
        }
        else if (hc.equals("rock") && cc.equals("scissors") ||
                hc.equals("paper") && cc.equals("rock") ||
                hc.equals("scissors") && cc.equals("paper")){
            return "Human wins!"; 
        }
        else {return "Computer wins!";
        }
    }

}
